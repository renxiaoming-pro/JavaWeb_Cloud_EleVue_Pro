// +----------------------------------------------------------------------
// | JavaWeb_Cloud_EleVue_Pro微服务前后端分离旗舰版框架 [ JavaWeb ]
// +----------------------------------------------------------------------
// | 版权所有 2021 上海JavaWeb研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.javaweb.vip/
// +----------------------------------------------------------------------
// | 作者: 鲲鹏 <javaweb520@gmail.com>
// +----------------------------------------------------------------------
// | 免责声明:
// | 本软件框架禁止任何单位和个人用于任何违法、侵害他人合法利益等恶意的行为，禁止用于任何违
// | 反我国法律法规的一切平台研发，任何单位和个人使用本软件框架用于产品研发而产生的任何意外
// | 、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、附带
// | 或衍生的损失等)，本团队不承担任何法律责任。本软件框架只能用于公司和个人内部的法律所允
// | 许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

package com.javaweb.auth.exception;

import com.alibaba.fastjson.JSONObject;
import com.javaweb.common.framework.utils.JsonResult;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.common.exceptions.InvalidGrantException;
import org.springframework.security.oauth2.common.exceptions.InvalidTokenException;
import org.springframework.security.oauth2.common.exceptions.OAuth2Exception;
import org.springframework.security.oauth2.provider.error.DefaultWebResponseExceptionTranslator;
import org.springframework.security.oauth2.provider.error.WebResponseExceptionTranslator;

public class CustomWebResponseExceptionTranslator extends DefaultWebResponseExceptionTranslator implements WebResponseExceptionTranslator<OAuth2Exception> {

    @Override
    public ResponseEntity<OAuth2Exception> translate(Exception e) throws Exception {
        ResponseEntity<OAuth2Exception> responseEntity = null;
        try {
            responseEntity = super.translate(e);
        } catch (Exception e1) {
            e1.printStackTrace();
        }
        JsonResult jsonResult = new JsonResult();
        OAuth2Exception body = responseEntity.getBody();
        if (e instanceof InvalidGrantException) {
            return new ResponseEntity(jsonResult.error(402, "用户名或密码不正确"), HttpStatus.OK);
        } else if (e instanceof InvalidTokenException) {
            return new ResponseEntity(jsonResult.error(401, "token已失效"), HttpStatus.OK);
        } else {
            return new ResponseEntity(jsonResult.error(401, body.getLocalizedMessage()), HttpStatus.OK);
        }

    }
}
